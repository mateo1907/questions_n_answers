<div class="wrap">
  <h2 id="plugintitle"><?php echo $title ?></h2>
  <h3>Dodaj odpowiedź do pytania</h3>
  <form class="" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
    <input type="hidden" name="nonce" value="<?php echo $nonce ?>">
    <input type="hidden" name="action" value="adminActionAddAnswer">

    <div class="">
      <label for="question">Pytanie</label>
      <select class="" name="question">
        <?php foreach ($questions as $q): ?>
          <option value="<?php echo $q->id ?> "><?php echo $q->question ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="">
      <label for="answer">Odpowiedź do pytania</label>
      <input type="text" name="answer" value="">
    </div>
    <div class="">
      <input class="button button-primary" type="submit" name="" value="Dodaj odpowiedź">

    </div>
  </form>
  <h3>Lista odpowiedzi</h3>
  <?php foreach ($answers as $a): ?>
    <p><?php echo $a->answer ?> - <?php echo $a->question->question ?></p>
  <?php endforeach; ?>
</div>
