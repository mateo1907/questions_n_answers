<div class="wrap">
  <h2 id="plugintitle"><?php echo $title ?></h2>
  <h3>Dodaj pytanie</h3>
  <form class="" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
    <input type="hidden" name="action" value="adminActionAddQuestion">
    <input type="hidden" name="nonce" value="<?php echo $nonce ?>">
    <div class="">
      <label for="question">Pytanie</label>
      <input type="text" name="question" value="">
    </div>
    <div class="">
      <input class="button button-primary" type="submit" name="" value="Dodaj pytanie">
    </div>
  </form>

  <h3>Lista dodanych pytań</h3>
  <?php foreach ($questions as $q): ?>
    <p><?php echo $q->question ?></p>
  <?php endforeach; ?>
</div>
