<?php
namespace Controllers;
use Models\Question;
use Models\Answer;

class AdminActionsController
{

  protected $functions = array();

  public function __construct()
  {
    $this->functions = get_class_methods($this);
  }
  public function init()
  {
    foreach ($this->functions as $function) {
      add_action('admin_post_'.$function,array($this,$function));
    }
  }

  public function adminActionAddQuestion()
  {
    if (wp_verify_nonce($_POST['nonce'],'addQuestion')) {
      $question = new Question;
      $question->question = sanitize_text_field($_POST['question']);
      if ($question->question) {
        $question->save();
        $query = http_build_query(['type'=>'success','msg'=>'Dodano pomyślnie']);
        exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
      } else {
        $query = http_build_query(['type'=>'error','msg'=>'Pole nie może zostać puste']);
        exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
      }

    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
    }
  }

  public function adminActionAddAnswer()
  {
    if (wp_verify_nonce($_POST['nonce'],'addAnswer')) {
      $question = Question::findOrFail((int)$_POST['question']);
      $answer = new Answer;
      $answer->answer = sanitize_text_field($_POST['answer']);
      $question->answers()->save($answer);
      $query = http_build_query(['type'=>'success','msg'=>'Dodano pomyślnie']);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    }
  }

}
