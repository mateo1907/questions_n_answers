<?php
namespace Controllers;
use Core\View;
use Models\Question;
use Models\Answer;

class AdminController
{

  public function init()
  {
    add_action('admin_menu',array($this,'createAdminMenu'));
  }


  public function createAdminMenu()
  {
    add_menu_page('Question&Answers','Question&Answers','manage_options','qa_home',array($this,'adminPluginBlockBody'));
    add_submenu_page('qa_home','Pytania','Pytania','manage_options','qa_questions',array($this,'adminQuestionsBlockBody'));
    add_submenu_page('qa_home','Odpowiedzi','Odpowiedzi','manage_options','qa_answers',array($this,'adminAnswersBlockBody'));
    add_submenu_page('qa_home','Statystyki','Statystyki','manage_options','qa_stats',array($this,'adminStatsBlockBody'));
  }

  /**
   * Metoda wyswietlajaca widok Pluginu
   * @return [type] [description]
   */
  public function adminPluginBlockBody()
  {

    $view = new View('admin.adminBlockBody');
    $view->setVariables('title','Question&Answers');
    echo $view->render();
  }

  public function adminQuestionsBlockBody()
  {
    $questions = Question::all();
    $nonce = wp_create_nonce('addQuestion');
    $view = new View('admin.adminQuestionsBlockBody');
    $view->setVariables('questions',$questions);
    $view->setVariables('nonce',$nonce);
    echo $view->render();
  }

  public function adminAnswersBlockBody()
  {
    $answers = Answer::all();
    $questions = Question::all();
    $nonce = wp_create_nonce('addAnswer');
    $view = new View('admin.adminAnswersBlockBody');
    $view->setVariables('answers',$answers);
    $view->setVariables('questions',$questions);
    $view->setVariables('nonce',$nonce);
    echo $view->render();
  }

  public function adminStatsBlockBody()
  {

  }
}
