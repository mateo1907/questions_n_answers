<?php

namespace Core;

use Controllers\AdminController;
use Controllers\AdminActionsController;
class Init
{
  public function init()
  {
    $admin = new AdminController;
    $admin->init();

    $adminActions = new AdminActionsController;
    $adminActions->init();
  }
}
